

class Game():

    def __init__(self, board):
        self.game = board

    def has_initialized(self):
        return self.game is not None

    def add_piece(self, player, row, column):
        """
        game: game state
        player: player number
        row: 0-index row
        column: 0-index column
        """
        raise Exception(
            "Agrega una jugada al tablero" +
            "\n debería usar set_position"
        )

    def moves_exist(self):

        raise Exception(
            "Tenes que implementar esta funcion que" +
            "\n devuelve True o False indicando si todavia hay algun lugar libre"
        )


    def check_winner(self):
        winner = 2
        raise Exception(
            "Tenes que implementar esta funcion que develve 0 o 1 si hay ganador " +
            "\n 0 para el jugador 1 y 1 para el jugador 2" +
            "\n en caso que todavia no exista un ganador, devolve 2"
        )
        return winner
