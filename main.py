from board import Board
from game import Game
from player import Player


class Main():

    def __init__(self, game=None, board=None, players=[]):
        self.game = game
        self.board = board
        self.players = players
        self.player_index = 0
        self.winner = 2  # the winner is not yet defined
        self.game.game.display_game()

    # go on forever
    def convert_input_to_coordinate(self, user_input):
        return user_input - 1

    def display_winner(self):
        if self.winner == 2:
            print("Tie")
        else:
            print("Player " + str(self.players[self.winner]) + " wins!")

    def switch_player(self):
        self.player_index += 1
        return self.player_index % 2

    def start(self):
        player = self.player_index
        while self.winner == 2 and self.game.moves_exist():
            print("Currently player: " + str(self.players[player]))
            available = False
            while not available:
                row = self.convert_input_to_coordinate(int(input("Elelgi una fila (del 1 al 3)")))
                column = self.convert_input_to_coordinate(int(input("Elelgi una columna (del 1 al 3)")))
                available = self.game.game.check_space_empty(row, column)

                if available:
                    self.game.add_piece(player, row, column)
                    self.game.game.display_game()

                    self.winner = self.game.check_winner()
                    player = self.switch_player()

        self.display_winner()


if __name__ == '__main__':

    b = Board()
    main = Main(
        game=Game(b), board=b, players=[Player('player1'), Player('player2')]
    )

    main.start()
