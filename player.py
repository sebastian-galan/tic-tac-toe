
class Player():

    def __init__(self, player_name):
        self.name = player_name

    def get_name(self):
        return self.name

    def __str__(self):
        return self.get_name()
