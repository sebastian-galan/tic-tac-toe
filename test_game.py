import unittest
from unittest.mock import patch
from board import Board
from game import Game

try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO


def side_effect_empty(x, y):
    """
    Los lugares vacios se representan con el numero entero 2
    Un tablero vacio es  una matriz cuyos elementos son todos 2
    """
    return 2


class TestPlayerPy(unittest.TestCase):

    """
    Game representa una partida de dos jugadores 
    usa Board como tablero y sus funciones para colocar
    las piezas de cada jugador

    Chequea si hay un ganador
    Chequea si todavia hay lugares para completar en el tablero
    """

    def setUp(self):
        b = Board()
        self.game = Game(b)


    @patch('board.Board.set_position')
    def test_add_piece(self, m_set_position):
        """ 
        Agrega una cruz o un circulo
        En este caso, player esta representado por un numero
        0 para jugador 1 y 1 para juador 2 
        add_piece(player, row, column)
        """
        self.game.add_piece(1,1,0)
        self.assertEqual(m_set_position.call_count, 1)

    @patch('board.Board.get_position')
    def test_moves_exist(self, m_get_position):
        """
        True si hay lugares en el tablero
        """

        m_get_position.side_effect = side_effect_empty

        self.assertEqual(self.game.moves_exist(), True)
        self.assertEqual(m_get_position.call_count, 1)


    def test_check_winner(self):

        """
        Si hay ganador devuelve 0 o 1 dependiendo el jugador
        Si no hay ganador devuelve 2 (como si fuera una celda vacia)

        """

        self.assertEqual(self.game.check_winner(), 2)

        self.game.add_piece(1,0,0)
        self.game.add_piece(1,1,1)
        self.game.add_piece(1,2,2)

        self.assertEqual(self.game.check_winner(), 1)


if __name__ == '__main__':
    unittest.main()
