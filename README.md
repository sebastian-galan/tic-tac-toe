##Ta-Te-Ti, suerte para mi!

- Dibuja un tablero de ta-te-ti en salida estándar
- Obtiene y agrega jugadas al tablero
- Chequea si alguno de los jugadores es ganador
- Indica a quien le corresponde la siguiente jugada

####Los componentes de este juego están divididos en 3 partes:

- Una clase que ayuda a construir una un tablero
- Una clase que representa una partida de Ta-Te-Ti de dos jugadores
- Una clase principal para ejecutar el juego

###Problema a resolver:
- Se ah perdido parte de la implementación y necesitamos completar algunas funciones para que el juego vuelva a funcionar correctamente.

- Para guiarnos en lo que nos falta agregamos algunos casos de prueba con
alguna especificación sobre el comportamiento que debería tener la función, sus posibles entradas y salidas

- Son varios los casos de prueba que en este momento fallan (mas de 15), con lo cual, tu tarea será guiarte con estos casos par completar las funcionalidades que faltan.

###Correr el programa

- Para correr el juego necesitas Python 3.5 y virtualenv y git para mantener los cambios

- Clonar este repositorio
- Crear un branch desde el branch latest, el nombre del branch será de la forma ```nombre-apellido```

- Para correr los test alcanza con ```python -m unittest```, o si queres correr de un test por vez (por ejemplo el de la clase Game) ``` python3  test_game.py``` 

- Para comenzar crea un entorno usando ```virtualenv```, por ejemplo ejecutando ```virtualenv -p python3 env```

###Envianos la solucion
- Una vez completado o avanzado, utilizando git podes proponernos un PR (Pull Request) con las solución que encontraste 

#####Como enviarnos el PR:
- Poné todos los commits que hagan falta en tu branch
- Envia tu branch  ultilizando push, la forma de hacerlo será  
```$ git push origin nombre-apellido ```

(aqui origin hace referencia a nuestro repositorio)






