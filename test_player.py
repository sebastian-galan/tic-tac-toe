import unittest
from player import Player


class TestPlayerPy(unittest.TestCase):

    def setUp(self):
        pass

    def test_print_player(self):
        '''
        Imprimir el nombre del jugador
        '''
        self.assertEqual(str(Player('Mi Nombre')), 'Mi Nombre')

    def test_implemented_str(self):
        '''
        Imprimir el nombre del jugador implementando __str__
        '''
        self.assertNotEqual(Player.__str__, object.__str__)

    def test_get_name_player(self):
        '''
        Tambien es necesario un getter
        '''
        p = Player('Mi Nombre')
        self.assertEqual(str(p.get_name()), 'Mi Nombre')

if __name__ == '__main__':
    unittest.main()
