import unittest
from unittest.mock import patch
from board import Board

try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO


class TestPlayerPy(unittest.TestCase):

    def setUp(self):
        self.board = Board()

    def test_empty(self):
        """
        Deberia retornar un tablero con todos los casilleros vacios.

        Para nosotros, un tablero vacio es una una matriz donde cada
        posicion tiene un numero 2 (2 indica que no hay ni un circulo ni una cruz)
        """
        empty_example = [[2, 2, 2] for x in range(3)]
        self.assertEqual(self.board.empty(), empty_example)

    def test_get_position(self):
        """
        Valores fuera de los indices de el tablero devuelven -1
        Valores dentro de indices del tablero devuelven vacio (2), cruz (1) o cirulo(2) 
        En este caso el test se hace con un tablero vacio
        """
        self.assertEqual(self.board.get_position(-1, 1), -1)
        self.assertEqual(self.board.get_position(1, -1), -1)
        self.assertEqual(self.board.get_position(1, 3), -1)
        self.assertEqual(self.board.get_position(3, -1), -1)
        self.assertEqual(self.board.get_position(1, 1), 2)
        self.assertEqual(self.board.get_position(2, 2), 2)
        self.assertEqual(self.board.get_position(0, 0), 2)

    def test_set_position(self):

        """
        Si agrego un valor, deberia recuperar el mismo mas tarde
        """
        self.board.set_position(0,0, 0)
        self.board.set_position(1,1, 0)
        self.board.set_position(2,2, 0)
        self.assertEqual(self.board.get_position(0, 0), 0)
        self.assertEqual(self.board.get_position(2, 2), 0)
        self.assertEqual(self.board.get_position(0, 0), 0)

    def test_get_col(self):
        """
        Devuelve una columna, indices de 0 a 2
        """
        self.board.set_position(0,1, 0)
        self.board.set_position(1,1, 0)
        self.board.set_position(2,1, 0)
        self.assertEqual(self.board.get_col(1), [0, 0, 0])
        self.assertEqual(self.board.get_col(0), [2, 2, 2])
        self.assertEqual(self.board.get_col(2), [2, 2, 2])

    def test_get_row(self):
        """
        Devuelve una Fila, indices de 0 a 2
        """
        self.board.set_position(1,0, 0)
        self.board.set_position(1,1, 0)
        self.board.set_position(1,2, 0)
        self.assertEqual(self.board.get_row(1), [0, 0, 0])
        self.assertEqual(self.board.get_row(0), [2, 2, 2])
        self.assertEqual(self.board.get_row(2), [2, 2, 2])

    def test_draw_line(self):
        """
        Esta funcion es util para imprimir el tablero
        Imprime una linea.
        """
        #self.assertEqual(self.board.draw_line(3, " ", "_"), '_ _ _')
        pass

    @patch('sys.stdout', new_callable=StringIO)
    def test_draw_columns(self, mock_stdout):
        """
        Esta funcion nos ayuda a armar una celda de una columna
        """
        self.board.draw_columns(' ')
        self.assertEqual(mock_stdout.getvalue(), '| |\n')

        self.board.draw_columns('X')
        self.assertEqual(mock_stdout.getvalue(), '| |\n|X|\n')

        self.board.draw_columns('O')
        self.assertEqual(mock_stdout.getvalue(), '| |\n|X|\n|O|\n')


    @patch('sys.stdout', new_callable=StringIO)
    def test_display_empty_game(self, mock_stdout):

        """
            Esta funcion muestra el estado del tablero en un momento determinado
            En este caso es cuando es vacio
             _ _ _
            |_|_|_|
            |_|_|_|
            |_|_|_|
        """
        self.board.display_game()
        self.assertEqual(mock_stdout.getvalue(), ' _ _ _ \n|_|_|_|\n|_|_|_|\n|_|_|_|\n')


    @patch('sys.stdout', new_callable=StringIO)
    def test_display_game_with_center(self, mock_stdout):
        """
            Esta funcion muestra el estado del tablero en un momento determinado
            En este caso es cuando es vacio
             _ _ _
            |X|_|_|
            |_|X|_|
            |_|_|X|
        """
        self.board.set_position(0,0,1)
        self.board.set_position(1,1,1)
        self.board.set_position(2,2,1)

        self.board.display_game()
        self.assertEqual(mock_stdout.getvalue(), ' _ _ _ \n|X|_|_|\n|_|X|_|\n|_|_|X|\n')

    def test_check_row_winner(self):
        """
        Si la lista esta llena de 1, entonces gano player 2
        Si la lista esta llena de 0 entonces gano el player 1
        """

        self.assertEqual(self.board.check_row_winner([2, 2, 2]), 2)
        self.assertEqual(self.board.check_row_winner([1, 1, 1]), 1)
        self.assertEqual(self.board.check_row_winner([0, 0 ,0]), 0)


    @patch('board.Board.get_position')
    def test_check_space_empty_use_get_position(self, m_get_method):
        """
        Valida si efectivamente es un lugar bacante. usa get_position 
        """
        self.board.set_position(0,0, 0)
        self.board.set_position(0,1, 1)
        self.board.check_space_empty(0,1)
        self.board.check_space_empty(2,1)
        self.assertEqual(m_get_method.call_count, 2)

    def test_check_space_empty(self):
        """
        Valida si efectivamente es un lugar bacante. usa get_position 
        """
        self.board.set_position(0,0, 0)
        self.board.set_position(0,1, 1)
        self.assertEqual(self.board.check_space_empty(0, 1), False)
        self.assertEqual(self.board.check_space_empty(0, 0), False)
        self.assertEqual(self.board.check_space_empty(2, 1), True)


if __name__ == '__main__':
    unittest.main()
