

class Board():

    def __init__(self):
        self.board = self.empty()

    def empty(self):
        return [[2, 2, 2] for x in range(3)]

    def get_position(self, row_num, col_num):
        result = -1

        raise Exception( "Tenes que implemetar esta funcion" +
            "\n que devuelve una el valor de una celda del tablero"
        )

        return result

    def set_position(self, row_num, col_num, value):
        raise Exception( "Tenes que implemetar esta funcion" +
            "\n que agrega una pieza en el tablero, en este caso un 1 o un 0"
        )
        #self.board[row_num][col_num] = value

    def get_col(self, col_number):
        raise Exception( "Tenes que implemetar esta funcion" +
            "\n que devuelve una columna del tablero"
        )

        result = [self.get_position(x, col_number) for x in range(3)]
        return result

    def get_row(self, row_number):
        raise Exception( "Tenes que implemetar esta funcion" +
            "\n que devuelve una fila del tablero"
        )

        return self.board[row_number]

    def draw_line(self, width, edge, filling):
        print(filling.join([edge] * (width + 1)))

    def draw_columns(self, new_row):
        print("|" + "|".join(new_row) + "|")

    def display_game(self):
        d = {0: "O", 1: "X", 2: "_"}

        self.draw_line(3, " ", "_")

        for row_num in range(3):
            new_row = []
            for col_num in range(3):
                new_row.append(
                    d[(self.get_position(row_num, col_num))]
                )

            self.draw_columns(new_row)

    def check_row_winner(self, row):
        if row[0] == row[1] and row[1] == row[2]:
            return row[0]
        return 2

    def check_space_empty(self, row, column):
        raise Exception( "Tenes que implemetar esta fucncion" +
            "\n que chequea si un casillero es vacio o no"
        )

        return self.get_position(row, column) == 2
